#!/bin/bash


echo "$(date +'%Y%m%d-%H%M%S')" > /tmp/timestamp.txt

cd /opt/rsx102
git pull --all -q --ff

# Synchronisation du code dans ~/sources/tp2
tp2_folder=~/sources/tp2/
tp2_source=/opt/rsx102/sources/tp2/* 
if [ ! -d ${tp2_folder} ]; then
    mkdir -p ${tp2_folder}
    cp -R ${tp2_source} ${tp2_folder}
elif [ ! -d ${tp2_folder}/01-UDP_client/Debug ]; then
    # Pas encore compilé, donc on peut écraser
    cp -Rf ${tp2_source} ${tp2_folder}
fi

#exit 0
