/*
 * Serveur UDP
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
//-------------------------------------------------------
//
//-------------------------------------------------------
#define PORTS 6260
typedef struct abonne {
    char nom[30];
    char telephone[11];
} t_abonne;
t_abonne annuaire[] = { {"abonne00","00.00.00.00"},
    {"abonne01","11.11.11.11"},
    {"abonne02","22.22.22.22"},
    {"abonne03","33.33.33.33"},
    {"abonne04","44.44.44.44"},
    {"abonne05","55.55.55.55"},
    {"abonne06","66.66.66.66"},
    {"abonne07","77.77.77.77"},
    {"abonne08","88.88.88.88"},
    {"abonne09","99.99.99.99"}
};
//-------------------------------------------------------
//
//-------------------------------------------------------
void print_sockaddr_in(char *txt, struct sockaddr_in ad)
{
    switch (ad.sin_family) {
    case AF_INET :
        printf("%s -> (AF_INET - @ : '%s' - port : '%d')\n", txt, inet_ntoa(ad.sin_addr), ntohs(ad.sin_port));
        break;
        // case AF_UNIX : printf("%s -> (AF_UNIX - @ : '%s' - port : '%d')\n", txt, inet_ntoa(ad.sin_addr), ntohs(ad.sin_port));
        break;
    default :
        printf("%s -> (domaine inconnu ...)\n", txt);
        break;
    }
    return;
}
//-------------------------------------------------------
//
//-------------------------------------------------------
void service(int nsock)
{
    struct sockaddr_in sin_client;
    char line[30];
    socklen_t lg_adresse;
    int r;
    int i;
    memset(line, '\0', sizeof(line));
    memset(&sin_client, '\0', sizeof(sin_client));
    r = recvfrom(nsock, line, 30, 0, (struct sockaddr *)&sin_client,(socklen_t *)&lg_adresse);
    printf("recv() : %d bytes of data in buf = '%s'\n", r, line);
    print_sockaddr_in("Client ", sin_client);
    i = 0;
    while (i < 10) {
        if (strncmp(line, annuaire[i].nom, r) == 0) {
            sendto(nsock, annuaire[i].telephone, 11, 0, (struct sockaddr *)&sin_client, sizeof(sin_client));
            return;
        }
        i = i + 1;
    }
    sendto(nsock, "erreur !!!!", 11, 0,(struct sockaddr *)&sin_client, sizeof(sin_client));
    return;
}
//-------------------------------------------------------
//
//-------------------------------------------------------
int main()
{
    struct sockaddr_in sin_serveur;
    int sock;
    int lg;
    struct hostent *h;
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket");
        exit(1);
    }
    printf("N° de socket : %d\n",sock);
    // récupération de l'adresse locale du PC
    if ((h = gethostbyname("localhost")) == NULL) {
        perror("gethostbyname");
        exit(3);
    }
    // préparation de l'adresse du serveur
    lg = sizeof(sin_serveur);
    memset(&sin_serveur, '\0', lg);
    sin_serveur.sin_family = AF_INET;
    sin_serveur.sin_port = htons(PORTS);
    //sin_serveur.sin_addr.s_addr = INADDR_ANY;
    memcpy(&sin_serveur.sin_addr, h->h_addr, h->h_length);
    print_sockaddr_in("Serveur", sin_serveur);
    if (bind(sock, (struct sockaddr *)&sin_serveur, (socklen_t)lg) < 0) {
        perror("bind");
        exit(2);
    }
    while (1) {
        service(sock);
    }
    exit(0);
}
