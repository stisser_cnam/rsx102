/*
 * Client UDP
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
//-------------------------------------------------------
//
//-------------------------------------------------------
#define PORTS 6260
#define PORTC 6259
//-------------------------------------------------------
//
//-------------------------------------------------------
void print_sockaddr_in(char *txt, struct sockaddr_in ad)
{
    switch (ad.sin_family) {
    case AF_INET :
        printf("%s -> (AF_INET - @ : '%s' - port : '%d')\n", txt, inet_ntoa(ad.sin_addr), ntohs(ad.sin_port));
        break;
    case AF_UNIX :
        printf("%s -> (AF_UNIX - @ : '%s' - port : '%d')\n", txt, inet_ntoa(ad.sin_addr), ntohs(ad.sin_port));
        break;
    default :
        printf("%s -> (domaine inconnu ...)\n", txt);
        break;
    }
    return;
}
//-------------------------------------------------------
//
//-------------------------------------------------------
int main()
{
    struct hostent *h;
    struct sockaddr_in sin_client;
    struct sockaddr_in sin_serveur;
    char abonne[30];
    char telephone[11];
    int sock;
    int lg;
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        perror("socket");
        exit(1);
    }
    printf("N° de socket : %d\n",sock);
    // récupération de l'adresse locale du PC
    if ((h = gethostbyname("localhost")) == NULL) {
        perror("gethostbyname");
        exit(3);
    }
    // préparation de l'adresse du client
    lg = sizeof(sin_client);
    memset(&sin_client, '\0', lg);
    sin_client.sin_family = AF_INET;
    sin_client.sin_port = htons(PORTC);
    memcpy(&sin_client.sin_addr, h->h_addr, h->h_length);

    print_sockaddr_in("Client ", sin_client);
    if (bind(sock, (struct sockaddr *)&sin_client, (socklen_t)lg) < 0) {
        perror("bind");
        exit(2);
    }
    // préparation de l'adresse du serveur
    lg = sizeof(sin_serveur);
    memset(&sin_serveur, '\0', lg);
    sin_serveur.sin_family = AF_INET;
    sin_serveur.sin_port = htons(PORTS);
    memcpy(&sin_serveur.sin_addr, h->h_addr, h->h_length);
    print_sockaddr_in("Serveur", sin_serveur);
    // saisi du nom de l'abonné recherché
    memset(abonne, '\0', sizeof(abonne));
    printf("Donnez un nom d'abonné : ");
    fgets(abonne, 30, stdin);
    // envoi de la requête au serveur
    sendto  (sock, abonne, strlen(abonne), 0, (struct sockaddr *)&sin_serveur, (socklen_t)lg);
    // réception de la réponse du serveur
    recvfrom (sock, telephone, sizeof(telephone), 0, (struct sockaddr *)&sin_serveur, (socklen_t *)&lg);
    // Affichage de la réponse
    if (strcmp(telephone, "erreur !!!!") == 0) {
        printf("Désolé, l\'abonné '%s' n\'existe pas\n", abonne);
    } else {
        printf("l\'abonné '%s' a pour N° de téléphone '%s'\n", abonne, telephone);
    }
    shutdown(sock, 2);
    exit(0);
}
